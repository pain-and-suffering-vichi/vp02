﻿!  VP_02.f90 
!
!  FUNCTIONS:
!  Производит интерполяцию для равномерной и чебышевой сетки.
!

!****************************************************************************
!
!  PROGRAM: VP_02
!
!  PURPOSE:  Чтение и вывод данных, выбор сетки интерполирования
!
!****************************************************************************

program VP_02
use inter
implicit none

integer(4) :: i, n
real(8) :: a, b
character(9) :: nettype
real(8), allocatable :: X(:), Y(:), Xk(:), Yk(:)

    call getarg(1,nettype)    

    if (nettype == 'uniform  ') then
        open(1,file='uniform.dat')
        open(2,file='res_uniform.dat',status='replace')
    else
        open(1,file='chebyshev.dat')
        open(2,file='res_chebyshev.dat',status='replace')
    endif
        
    read(1,'(2x,I6)') n
        allocate(X(0:n))
        allocate(Y(0:n))
        allocate(Xk(0:100*n))
        allocate(Yk(0:100*n))
    read(1,*) a, b
    read(1,*) Y
    
    if (nettype == 'uniform') then
        call UniformInterpolation(a,b,n,Xk,Yk,X,Y)
    else
        call CebyshevInterpolation(a,b,n,Xk,Yk,X,Y)
    endif
    
    do i=0,100*n
        write(2,*) Xk(i), Yk(i)
    enddo    
    
    close(1)
    close(2)

end program VP_02

